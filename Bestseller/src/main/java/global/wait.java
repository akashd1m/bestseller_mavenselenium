package global;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;

public class wait {
	// Delay for custom seconds
	public static void DelayForSeconds(WebDriver driver, Integer seconds) {
		driver.manage().timeouts().implicitlyWait(seconds, TimeUnit.SECONDS);
	}
	// Sleep for custom seconds
	public static void SleepToSeconds(Integer seconds) throws InterruptedException {	
		System.out.println("waiting for" + seconds + " seconds");
		TimeUnit.SECONDS.sleep(seconds);
	}
}
