package global;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

public class alerts {
	public static void fewSecondsAlertWithCustomMessage(WebDriver driver, JavascriptExecutor js, String message, Integer seconds) throws InterruptedException {
		js.executeScript("alert(" + message + ");");
	
		Alert alert=driver.switchTo().alert();
		
		TimeUnit.SECONDS.sleep(seconds);
		alert.accept();
	}	
}
