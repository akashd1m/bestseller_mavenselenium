package global;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class button {
	// WebDriver Waits Until The Button is Visible for Click
	public static Object waitUntilTheButtonIsVisible(WebDriver driver, String buttonClassName) {
		
		WebDriverWait waitTime = new WebDriverWait(driver, 10);
		WebElement waitButtonElement = waitTime.until(ExpectedConditions.elementToBeClickable(By.className(buttonClassName)));
		
		if (waitButtonElement.isEnabled()) {
			waitButtonElement.click();
		}
		return true;
	}
}
