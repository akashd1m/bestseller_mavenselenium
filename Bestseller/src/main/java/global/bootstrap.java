package global;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

public class bootstrap {
	
	public static Object callBootstrapModalWithNoButtonsForTwoSeconds(WebDriver driver, JavascriptExecutor js, String action_title, String action_name) throws Exception {
		createBootstrapModalWithNoButtons(driver, js, action_title, action_name);
		includeScriptForTypingEffect(js);
		displayModal(js);
		positionModalAtFixedCenterOfWindow(js);
		js.executeScript("window.jQuery('#next-action-modal').removeClass('d-none');");
		wait.SleepToSeconds(4);
		return true;
	}
	
	public static Object createBootstrapModalWithNoButtons(WebDriver driver, JavascriptExecutor js, String action_title, String action_name) throws InterruptedException {
		return (Object) js.executeScript("window.jQuery('body').append(\"<div id='next-action-modal' class='action-modal bootstrap-modal d-none' tabindex='-1' role='dialog'>" + 
				"  <div class='modal-dialog' role='document'>" + 
				"    <div class='modal-content'>" + 
				"      <div class='modal-header'>" + 
				"        <h5 class='modal-title'>" + action_title + "</h5>" + 
				"        <button type='button' class='close' data-dismiss='modal' aria-label='Close'>" + 
				"          <span aria-hidden='true'>�</span>" + 
				"        </button>" + 
				"      </div>" + 
				"      <div class='modal-body'>" + 
				"        <p><span id='caption'></span><span id='cursor'>|</span></p> <input type='hidden' id='next-caption' value='" + action_name + " ' />" + 
				"      </div>" + 
				"    </div>" + 
				"  </div>" + 
				"</div>\");"); 
	}
	
	public static Object createBootstrapModalWithButtonsYesOrNo(WebDriver driver, JavascriptExecutor js, String action_title, String action_name) throws InterruptedException {
		return (Object) js.executeScript("return window.jQuery('body').append(\"<div id='next-action-modal' class='action-modal bootstrap-modal d-none' tabindex='-1' role='dialog'>" + 
				"  <div class='modal-dialog' role='document'>" + 
				"    <div class='modal-content'>" + 
				"      <div class='modal-header'>" + 
				"        <h5 class='modal-title'>" + action_title + "</h5>" + 
				"        <button type='button' class='close' data-dismiss='modal' aria-label='Close'>" + 
				"          <span aria-hidden='true'>�</span>" + 
				"        </button>" + 
				"      </div>" + 
				"      <div class='modal-body'>" + 
				"        <p><span id='caption'></span><span id='cursor'>|</span></p> <input type='hidden' id='next-caption' value='" + action_name + " ' />" + 
				"      </div>" + 
				"      <div class='modal-footer'>" + 
				"        <button type='button' class='btn btn-primary'>Save changes</button>" + 
				"        <button type='button' class='btn btn-secondary' data-dismiss='modal'>Close</button>" + 
				"      </div>" + 
				"    </div>" + 
				"  </div>" + 
				"</div>\");"); 
	}
	
	public static Object displayModal(JavascriptExecutor js) {
		return js.executeScript("window.jQuery('#next-action-modal').modal('show');");	
	}
	
	public static Object hideModal(JavascriptExecutor js) {
		return js.executeScript("window.jQuery('#next-action-modal').modal('hide');");
	}
	
	public static Object positionModalAtFixedCenterOfWindow(JavascriptExecutor js) {
		return js.executeScript("window.jQuery('#next-action-modal').css({" + 
				"   'z-index' : '100000'," + 
				"   'position' : 'fixed'," + 
				"   'top' : '40%'," +
				"   'left' : '40%'," +
				"	'padding-right': '0'" + 
				"});");
	}
	
	public static Object includeCSSForTypingEffect(JavascriptExecutor js) throws Exception{
		 
		return js.executeScript("window.jQuery('body').append(\"<style>" + 
				"*, *:after, *:before {" + 
				"	box-sizing: border-box;" + 
				"	margin: 0;" + 
				"	padding: 0;" + 
				"}" + 
				"" +  
				".modal-dialog{" + 
				"	margin: 0;" + 
				"}" +
				".modal-body{" + 
				"	width: calc(20ch + 20*0.0625em);" + 
				"}" + 
				".modal-body p {" + 
				"	position: relative;" + 
				"	color: #152860;" + 
				"	text-align: center;" + 
				"	white-space: nowrap;" + 
				"	font-size: 40px;" +  
				"	width: calc(20ch + 20*0.0625rem);" + 
				"}" + 
				"" + 
				".modal-body p:after {" + 
				"	display: block;" + 
				"	position: absolute;" + 
				"	content: '';" + 
				"	width: calc(20ch + 20*0.0625em);" + 
				"	height: 7ex;" + 
				"	left: 0;" + 
				"	top: 0;" + 
				"	background: #eff5ed;" + 
				"	animation: 0.6s blink 20, move 3s 1.5s forwards steps(1);" + 
				"	border-left: 1px solid #152860;" + 
				"	box-shadow: 0px 1ex 0px 0px #eff5ed;" + 
				"}" + 
				"" + 
				"@keyframes move {" + 
				"	5% {" + 
				"		width: calc(calc(20ch + 20*0.0625em) - (1ch + 1*0.0625em));" + 
				"		left: calc(1ch + 1*0.0625em);" + 
				"	}" + 
				"" + 
				"	10% {" + 
				"		width: calc(calc(20ch + 20*0.0625em) - (2ch + 2*0.0625em));" + 
				"		left: calc(2ch + 2*0.0625em);" + 
				"	}" + 
				"" + 
				"	15% {" + 
				"		width: calc(calc(20ch + 20*0.0625em) - (3ch + 3*0.0625em));" + 
				"		left: calc(3ch + 3*0.0625em);" + 
				"	}" + 
				"" + 
				"	20% {" + 
				"		width: calc(calc(20ch + 20*0.0625em) - (4ch + 4*0.0625em));" + 
				"		left: calc(4ch + 4*0.0625em);" + 
				"	}" + 
				"" + 
				"	25% {" + 
				"		width: calc(calc(20ch + 20*0.0625em) - (5ch + 5*0.0625em));" + 
				"		left: calc(5ch + 5*0.0625em);" + 
				"	}" + 
				"" + 
				"	30% {" + 
				"		width: calc(calc(20ch + 20*0.0625em) - (6ch + 6*0.0625em));" + 
				"		left: calc(6ch + 6*0.0625em);" + 
				"	}" + 
				"" + 
				"	35% {" + 
				"		width: calc(calc(20ch + 20*0.0625em) - (7ch + 7*0.0625em));" + 
				"		left: calc(7ch + 7*0.0625em);" + 
				"	}" + 
				"" + 
				"	40% {" + 
				"		width: calc(calc(20ch + 20*0.0625em) - (8ch + 8*0.0625em));" + 
				"		left: calc(8ch + 8*0.0625em);" + 
				"	}" + 
				"" + 
				"	45% {" + 
				"		width: calc(calc(20ch + 20*0.0625em) - (9ch + 9*0.0625em));" + 
				"		left: calc(9ch + 9*0.0625em);" + 
				"	}" + 
				"" + 
				"	50% {" + 
				"		width: calc(calc(20ch + 20*0.0625em) - (10ch + 10*0.0625em));" + 
				"		left: calc(10ch + 10*0.0625em);" + 
				"	}" + 
				"" + 
				"	55% {" + 
				"		width: calc(calc(20ch + 20*0.0625em) - (11ch + 11*0.0625em));" + 
				"		left: calc(11ch + 11*0.0625em);" + 
				"	}" + 
				"" + 
				"	60% {" + 
				"		width: calc(calc(20ch + 20*0.0625em) - (12ch + 12*0.0625em));" + 
				"		left: calc(12ch + 12*0.0625em);" + 
				"	}" + 
				"" + 
				"	65% {" + 
				"		width: calc(calc(20ch + 20*0.0625em) - (13ch + 13*0.0625em));" + 
				"		left: calc(13ch + 13*0.0625em);" + 
				"	}" + 
				"" + 
				"	70% {" + 
				"		width: calc(calc(20ch + 20*0.0625em) - (14ch + 14*0.0625em));" + 
				"		left: calc(14ch + 14*0.0625em);" + 
				"	}" + 
				"" + 
				"	75% {" + 
				"		width: calc(calc(20ch + 20*0.0625em) - (15ch + 15*0.0625em));" + 
				"		left: calc(15ch + 15*0.0625em);" + 
				"	}" + 
				"" + 
				"	80% {" + 
				"		width: calc(calc(20ch + 20*0.0625em) - (16ch + 16*0.0625em));" + 
				"		left: calc(16ch + 16*0.0625em);" + 
				"	}" + 
				"" + 
				"	85% {" + 
				"		width: calc(calc(20ch + 20*0.0625em) - (17ch + 17*0.0625em));" + 
				"		left: calc(17ch + 17*0.0625em);" + 
				"	}" + 
				"" + 
				"	90% {" + 
				"		width: calc(calc(20ch + 20*0.0625em) - (18ch + 18*0.0625em));" + 
				"		left: calc(18ch + 18*0.0625em);" + 
				"	}" + 
				"" + 
				"	95% {" + 
				"		width: calc(calc(20ch + 20*0.0625em) - (19ch + 19*0.0625em));" + 
				"		left: calc(19ch + 19*0.0625em);" + 
				"	}" + 
				"" + 
				"	100% {" + 
				"		width: calc(calc(20ch + 20*0.0625em) - (20ch + 20*0.0625em));" + 
				"		left: calc(20ch + 20*0.0625em);" + 
				"	}" + 
				"}" + 
				"" + 
				"@keyframes blink {" + 
				"	0% {" + 
				"		border-left-color: #152860;" + 
				"	}" + 
				"" + 
				"	50% {" + 
				"		border-left-color: #152860;" + 
				"	}" + 
				"" + 
				"	51% {" + 
				"		border-left-color: #eff5ed;" + 
				"	}" + 
				"" + 
				"	100% {" + 
				"		border-left-color: #eff5ed;" + 
				"	}" + 
				"}" + 
				"</style>\");");
	}
	
	public static Object includeScriptForTypingEffect(JavascriptExecutor js) throws Exception{
		
		return js.executeScript("window.jQuery('body').append(\"<script> " +
				"var captionLength = 0;" + 
				"var caption = \'\';" +
				"$(function() {" + 
				"    setInterval (\'cursorAnimation()\', 600);" +
				"	 captionEl = $('#caption');" + 
				"    " + 
				"        testTypingEffect();" + 
				"});" + 
				"function testTypingEffect() {" + 
				"    caption = $('input#next-caption').val();" + 
				"    type();" + 
				"}" + 
				"" + 
				"function type() {" + 
				"    captionEl.html(caption.substr(0, captionLength++));" + 
				"    if(captionLength < caption.length+1) {" + 
				"        setTimeout('type()', 50);" + 
				"    } else {" + 
				"        captionLength = 0;" + 
				"        caption = \'\';" + 
				"    }" + 
				"}" + 
				"" + 
				"" + 
				"" + 
				"function cursorAnimation() {" + 
				"    $('#cursor').animate({" + 
				"        opacity: 0" + 
				"    }, 'fast', 'swing').animate({" + 
				"        opacity: 1" + 
				"    }, 'fast', 'swing');" + 
				"}" +
				"</script>\");");
			
	}
}
