package bestseller.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import Selenium.Bestseller.Page;

public class DressSectionPage extends Page {
	
	public void goToProductViewMoreOnParticularProduct()
	{
		WebElement element = driver.findElement(By.cssSelector(OR.getProperty("productElement")));
		Actions action = new Actions(driver);
		action.moveToElement(element).build().perform();
		driver.findElement(By.linkText(OR.getProperty("viewMoreLink"))).click();
		
		
	}
	
	public void toVerifyAddToBagDisbaledWithoutSelectSize()
	{
		WebElement element = driver.findElement(By.cssSelector(OR.getProperty("productElement")));
		Actions action = new Actions(driver);
		action.moveToElement(element).build().perform();
		isElementEnabledOrDisabled(By.linkText("ADD TO BAGs"));
		/*WebElement demodiv = driver.findElement(By.cssSelector(OR.getProperty("productElement")));
		System.out.println(demodiv.getAttribute("innerHTML"));
		
		System.out.println(demodiv.getAttribute("textcontent"));
	*/
	}
	
	public void toVerifyAddToBagEnabledWithSelectSize()
	{
		WebElement element = driver.findElement(By.cssSelector(OR.getProperty("productElement")));
		Actions action = new Actions(driver);
		action.moveToElement(element).build().perform();
		List<WebElement> options = driver.findElements(By.cssSelector("select.custom-select.form-control.select-size"));
		
 		for(WebElement sizeOption : options)
 		{
 			if(sizeOption.getText().equals("XS"));
 			System.out.println(sizeOption.getText());
 			sizeOption.click();
 			
 		}
 		isElementEnabledOrDisabled(By.linkText("ADD TO BAGS"));
		
	}
}
