package base.bestseller;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import global.*;

public class PlaceOrderWithRegisteredCustomer {
	
public static void main(String[] args) throws Exception {
		
		System.setProperty("webdriver.chrome.driver", "D:\\Projects\\Selenium\\selenium-java-3.7.1\\chromedriver_win32\\chromedriver.exe");
	
		WebDriver driver = new ChromeDriver();
		//driver.get("https://development-ap01-bestsellerfashiongroup.demandware.net/s/MYVM/home"); 
		driver.get("https://dev01-ap01-bestsellerfashiongroup.demandware.net/on/demandware.store/Sites-MYVM-Site/en_MY/Home-Show");
		
		// Vero moda Development 
		//driver.get("https://development-ap01-bestsellerfashiongroup.demandware.net/on/demandware.store/Sites-MYVM-Site/ms/Home-Show");
		
		// Vero moda Staging 
		//driver.get("https://staging-ap01-bestsellerfashiongroup.demandware.net/on/demandware.store/Sites-MYVM-Site/ms/Home-Show");
		
		// Jack Jones Development 
		//driver.get("https://development-ap01-bestsellerfashiongroup.demandware.net/on/demandware.store/Sites-HKJJ-Site/en_MY/Home-Show");
		
		
		// Login User 
			
		//JavascriptExecutor js = (JavascriptExecutor) driver;
		
	    //driver.get("https://dev01-ap01-bestsellerfashiongroup.demandware.net/on/demandware.store/Sites-MYVM-Site/en_MY/Home-Show");
	    driver.findElement(By.className("user")).click();
	    
	    // Wait for 4 seconds
	    wait.DelayForSeconds(driver, 10);
	    
	    driver.findElement(By.id("login-form-email")).clear();
	    driver.findElement(By.id("login-form-password")).clear();
	    driver.findElement(By.id("login-form-email")).sendKeys("l232296@nwytg.com");
	    
	    driver.findElement(By.id("login-form-password")).sendKeys("Lucifer@101");
	    
	  
	    wait.SleepToSeconds(2);

	    driver.findElement(By.xpath("//button[@type='submit']")).click();
		
	    TimeUnit.SECONDS.sleep(2);
	 	
	    TimeUnit.SECONDS.sleep(3);
	    
	   wait.SleepToSeconds(3);
	    
//	    bootstrap.callBootstrapModalWithNoButtonsForTwoSeconds(driver, js, "Next Action", "Search A Keyword");
	    
	    wait.DelayForSeconds(driver, 10);
	    
		WebElement element1 = driver.findElement(By.className("search__toggle"));
	
		JavascriptExecutor executor = (JavascriptExecutor) driver;	
		executor.executeScript("arguments[0].click();", element1);
	
		// search keyword dresses
		WebElement searchField = driver.findElement(By.className("search-field"));
		searchField.sendKeys("dresses");
		searchField.submit();
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		// click on a product tile
		//driver.findElement(By.xpath("//img[@alt='COCONUT DENIM DRESS(NC)']")).click();
		
		// development instance 
		driver.findElement(By.xpath("//img[@alt='VIOLA 1/2 DRESS(VMC-NN)']")).click();
		
		// development Jack Jones instance
		//driver.findElement(By.xpath("//img[@alt='O HARPER TEE B S/S(SLIM FIT)']")).click();
		
	
		driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
	 
		// click on view more button to redirect PDP page
		driver.findElement(By.linkText("VIEW MORE")).click();
	
		driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
	
		// select size variant
		//List < WebElement > li = driver.findElements(By.className("attribute-size"));
		
		  //----------------------------------------------------------------------------------
          //-- Checkout Forms: Shipping, Payment, Coupons, Billing, etc --
          //-----------------------------------------------------------------------------------
          
		// select size variant directly
		driver.findElement(By.linkText("S")).click();
		//((By) li).findElement((SearchContext) By.linkText("XS")).click();
	
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	
		// click on add-to-cart button when add-to-cart is enabled 
		waitUntilTheButtonIsVisible(driver, "add-to-cart");

		// click on view bag button to redirect to cart page
		driver.findElement(By.linkText("VIEW BAG")).click();

		// Wait for 4 seconds
		implicitlyWaitFor4Seconds(driver);

		// click on Checkout Now button to redirect to Guest Checkout page
		driver.findElement(By.linkText("CHECKOUT NOW")).click();

		// Wait for 4 seconds
		//implicitlyWaitFor4Seconds(driver);

		// click on Checkout Now button to redirect One Page Checkout
		//driver.findElement(By.linkText("CHECKOUT")).click();
		
		// Wait for 4 seconds
		implicitlyWaitFor4Seconds(driver);
		
		// fill out the input values to the shipping address fields
		//addShippingAddress(driver);

		// click on Save your address button to save shipping address
		//waitUntilTheButtonIsVisible(driver, "submit-shipping");

		// Wait for 4 seconds
		implicitlyWaitFor4Seconds(driver);
			
		// fill out the input values to the credit cards payments details
		enterCreditCardDetails(driver);
		
		// click on place Order button to place order
		waitUntilTheButtonIsVisible(driver, "place-order");
		
        //driver.close();
	}

	public static Object implicitlyWaitFor4Seconds(WebDriver driver) {
		return driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
	}
	
	public static Object waitUntilTheButtonIsVisible(WebDriver driver, String buttonClassName) {
		
		WebDriverWait waitTime = new WebDriverWait(driver, 10);

		WebElement waitButtonElement = waitTime.until(ExpectedConditions.elementToBeClickable(By.className(buttonClassName)));

		if (waitButtonElement.isEnabled()) {
			waitButtonElement.click();
		}
		return true;
	}

	public static Object addShippingAddress(WebDriver driver) {
		// fill out the input values to the shipping address fields
		driver.findElement(By.name("dwfrm_shipping_shippingAddress_addressFields_firstName")).sendKeys("JOHN");
		driver.findElement(By.name("dwfrm_shipping_shippingAddress_addressFields_lastName")).sendKeys("DOE");
		driver.findElement(By.name("dwfrm_shipping_shippingAddress_addressFields_email")).sendKeys("john.doe@gmail.com");
		driver.findElement(By.name("dwfrm_shipping_shippingAddress_addressFields_postalCode")).sendKeys("23185");
		driver.findElement(By.name("dwfrm_shipping_shippingAddress_addressFields_address1")).sendKeys("1401 MAIN ST");
		driver.findElement(By.name("dwfrm_shipping_shippingAddress_addressFields_address2")).sendKeys("FALLS CHURCH");
		driver.findElement(By.name("shippingPhoneNumber_phone")).sendKeys("2022682444");
		
		Select stateCode = new Select(driver.findElement(By.name("dwfrm_shipping_shippingAddress_addressFields_states_stateCode")));
		stateCode.selectByVisibleText("Kuala Lumpur");

		Select cityCode = new Select(driver.findElement(By.name("dwfrm_shipping_shippingAddress_addressFields_city")));
		cityCode.selectByVisibleText("Kuala Lumpur");

		return driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
		
	}
	
	public static Object enterCreditCardDetails(WebDriver driver) {
		// fill out the input values to the credit cards payments details
		driver.findElement(By.name("dwfrm_billing_creditCardFields_cardOwner")).sendKeys("JOHN DOE");
		driver.findElement(By.name("dwfrm_billing_creditCardFields_cardNumber")).sendKeys("4111111111111111");

		Select cmonth = new Select(driver.findElement(By.name("dwfrm_billing_creditCardFields_expirationMonth")));
		cmonth.selectByVisibleText("08");

		Select cyear = new Select(driver.findElement(By.name("dwfrm_billing_creditCardFields_expirationYear")));
		cyear.selectByVisibleText("2018");

		driver.findElement(By.name("dwfrm_billing_creditCardFields_securityCode")).sendKeys("737");

		return driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
		
	}

}




