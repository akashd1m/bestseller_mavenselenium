package base.bestseller;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import bestseller.pages.DressSectionPage;
import bestseller.pages.HomePage;

public class ProductAddCartTest extends BaseTest {

	@Test(priority = 1)
	public void addCart() {
		SoftAssert softAssertion = new SoftAssert();
		HomePage home = new HomePage();
		// home.goToDressBuysection();
		home.goToDressBuysection();
		softAssertion.assertEquals("Dress | Vero Moda Malaysia", driver.getTitle());
		softAssertion.assertAll();
		// Assert.assertTrue(condition);

	}
	
	@Test(priority = 2)
	public void verifyAddToBagDisbaledWithoutSelectSize()
	{
		
		DressSectionPage dressSection = new DressSectionPage();
		dressSection.toVerifyAddToBagDisbaledWithoutSelectSize();
		
		
	}
	@Test(priority=3)
	public void demo()
	{
		
		DressSectionPage dressSection = new DressSectionPage();
		dressSection.goToProductViewMoreOnParticularProduct();
	}

}
